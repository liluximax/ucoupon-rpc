#!/bin/bash
today=`date +%F`
yesterday=`date -d '1 days ago' +%F`
log_path='/root/ucoupon-rpc/nohup.out'

echo "执行日志备份日期：${today}"

if [ ! -f ${log_path} ]
then
echo "${log_path} not exist the shell will exit \n"
exit 0
fi

echo '备份开始...'
backup_name="rpc_log-${yesterday}.log"
backup_path="/root/ucoupon-rpc/rpc-log"
echo "backup -- name:${backup_name} path:${backup_path}"

cp ${log_path} ${backup_path}
cd ${backup_path}
mv nohup.out	${backup_name}

echo "${today}" > "${log_path}"
echo -e "备份完成\n"
