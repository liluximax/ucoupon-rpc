package cn.ucoupon.rpc;

import cn.ucoupon.rpc.backend.SleepUtil;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Created by luc on 17/5/7.
 */
public class GuavaCache {

    private static Cache<Integer, String> cache = CacheBuilder.newBuilder()
            .expireAfterWrite(5, TimeUnit.SECONDS)
            .removalListener(new RemovalListener<Object, Object>() {
                @Override
                public void onRemoval(RemovalNotification<Object, Object> removalNotification) {
                    System.out.println("remove: " + removalNotification.getKey() + removalNotification.getCause().name());
                }
            })
            .maximumSize(10)
            .build();

    public static void main(String[] args) {
        GuavaCache test = new GuavaCache();
        for(int i = 0; i < 10; i++){
            System.out.println(test.get(i));
        }
//        cache.invalidate(1);
//        System.out.println(cache.getIfPresent(1));
//        for(int i = 9; i >= 0; i--){
//            System.out.println(test.get(i));
//        }
        SleepUtil.sleep(10);
        cache.getIfPresent(1);
//        for(int i = 0; i < 10; i++){
//            System.out.println(test.get(i));
//        }
    }

    public String get(int key) {
        String res = null;
        try {
            res = cache.get(key, new Callable<String>() {
                @Override
                public String call() throws Exception {
                    return getFromRpc(key);
                }
            });
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return res;
    }

    public String getFromRpc(int key) {
        System.out.println("get do not from cache");
        return "--> " + key;
    }
}
