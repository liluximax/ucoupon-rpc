package cn.ucoupon.rpc.backend;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Created by luc on 17/5/6.
 */
public class MsgContainer {

    //key是uuid  生命周期是1小时
    private static Cache<String, BoxMsg> msgCache = CacheBuilder.newBuilder()
            .expireAfterWrite(1, TimeUnit.HOURS)
            .removalListener(new RemovalListener<Object, Object>() {
                @Override
                public void onRemoval(RemovalNotification<Object, Object> removalNotification) {
                    String uuid = (String) removalNotification.getKey();
                    int type = Integer.valueOf(uuid.split("\\|")[0]);
                    long id = Long.valueOf(uuid.split("\\|")[1]);
                    String casuse = removalNotification.getCause().name();
                    //主动删除cache中msg
                    if(casuse.equals("EXPLICIT")){
                        logger.info("remove [{}] msg from cache", uuid);
                    }
                    else {
                        logger.info("remove [{}] msg from cache by guava", uuid);
                    }
                }
            })
            .build();

    private static Logger logger = LogManager.getLogger();

    private static MsgContainer instance = new MsgContainer();

    public static MsgContainer getInstance() {
        return instance;
    }

    public MsgContainer() {
    }

    public boolean putMsg(BoxMsg msg, String uuid) {
        boolean flag = false;
        BoxMsg msgInCache = msgCache.getIfPresent(uuid);
        if(msgInCache == null){
            msgCache.put(uuid, msg);
            flag = true;
        }
        return flag;
    }

    public List<BoxMsg> getMsgById(Integer boxId) {
        List<BoxMsg> res = new ArrayList<>();
        ConcurrentMap<String, BoxMsg> map = msgCache.asMap();
        for(Map.Entry<String, BoxMsg> entry : map.entrySet()){
            BoxMsg msg = entry.getValue();
            if(msg.getBoxId() == boxId){
                res.add(msg);
            }
        }
        return res;
    }

    public int getMsgCount() {
        return (int) msgCache.size();
    }

    //删除内存队列中 已经处理过的消息
    public boolean removeMsgByIdAndType(String uuid) {
        msgCache.invalidate(uuid);
        return true;
    }

    public boolean containsMsgByUUid(String uuid) {
        BoxMsg msg = msgCache.getIfPresent(uuid);
        return msg != null ? true : false;
    }

    public void removeAllElement(){
        logger.info("prepare to remove all element from cache");
        msgCache.invalidateAll();
    }
}
