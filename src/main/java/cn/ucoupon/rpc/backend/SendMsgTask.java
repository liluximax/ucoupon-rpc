package cn.ucoupon.rpc.backend;

import io.netty.channel.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Created by luc on 17/5/6.
 */
public class SendMsgTask implements Runnable {

    private MsgContainer msgContainer = MsgContainer.getInstance();
    private BoxContainer boxContainer = BoxContainer.getInstance();
    private static Logger logger = LogManager.getLogger();
    private int boxId;
    private Channel channel;

    private volatile boolean isRunning = true;

    public SendMsgTask(int boxId) {
        this.boxId = boxId;
        this.channel = boxContainer.get(boxId);
    }

    public void stop() {
        this.isRunning = false;
    }

    @Override
    public void run() {

        SleepUtil.sleep(1);
        logger.info("[" + boxId + "] send thread is running, msg num is [" + msgContainer.getMsgCount() + "]");
        while (isRunning) {
            boolean state = boxContainer.checkHeartBeat(boxId);
            if(state){
                List<BoxMsg> sendMsg = msgContainer.getMsgById(boxId);
                if (sendMsg.size() > 0) {
                    for (BoxMsg send : sendMsg) {
                        if(msgContainer.containsMsgByUUid(send.getUuid())){
                            channel.writeAndFlush(send.getMsg() + "\n");
                            logger.info("box[{}] send msg [{}]", boxId, send.getUuid());
                            SleepUtil.sleep(3);
                        }
                    }
                }
                SleepUtil.sleep(3);
            }
        }
        logger.info(boxId + " task is stop");
    }

    public int getBoxId() {
        return boxId;
    }
}
