package cn.ucoupon.rpc.backend.monitor;

import cn.ucoupon.rpc.backend.MsgContainer;
import cn.ucoupon.rpc.db.QueryUtil;
import cn.ucoupon.rpc.db.impl.QueryOrderImpl;
import cn.ucoupon.rpc.db.pojo.UcouponMsg;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by luc on 17/5/10.
 */
public class MonitorPrintTask extends MonitorTask {
    private static Logger logger = LogManager.getLogger();
    private QueryUtil queryUtil = new QueryOrderImpl();
    private static volatile boolean isRunning = true;
    private MsgContainer msgContainer = MsgContainer.getInstance();
    private static int beats = 10;
    @Override
    public void run() {
        while (isRunning) {
            monitor_oil_transaction_query();
            monitor_shop_transaction_query();
            try {
                Thread.sleep(beats * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void monitor_tb(int type) {
        List<UcouponMsg> orders = new ArrayList<>();
        String tb_name = new String();
        switch (type) {
            case 2:
                orders = queryUtil.queryOilPrintInfo();
                tb_name = "oil_transaction_query";
                break;
            case 4:
                orders = queryUtil.queryGoodsPrintInfo();
                tb_name = "shop_transaction_query";
                break;
        }
        addMsgToCache(orders, type);
//        logger.info("scan [{}] thread is running, msg num is [{}], [{}]/per", tb_name, msgContainer.getMsgCount(), beats);
    }

    public void monitor_oil_transaction_query() {
        monitor_tb(2);
    }

    public void monitor_shop_transaction_query() {
        monitor_tb(4);
    }

    void stop() {
        this.isRunning = false;
    }
}
