package cn.ucoupon.rpc.backend.monitor;

import cn.ucoupon.rpc.backend.MsgContainer;
import cn.ucoupon.rpc.db.QueryUtil;
import cn.ucoupon.rpc.db.impl.QueryOrderImpl;
import cn.ucoupon.rpc.db.pojo.UcouponMsg;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by luc on 17/5/8.
 */
public class MonitorOilOrderTask extends MonitorTask {

    private static Logger logger = LogManager.getLogger();
    private QueryUtil queryUtil = new QueryOrderImpl();
    private static volatile boolean isRunning = true;
    private static int beats = 5;
    private MsgContainer msgContainer = MsgContainer.getInstance();

    @Override
    public void run() {
        while (isRunning) {
            monitor_oil_trasaction_tb();
            monitor_transaction_test();
            monitor_shop_transaction_info_2();
            try {
                Thread.sleep(beats * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void monitor_tb(int type) {
        List<UcouponMsg> orders = new ArrayList<>();
        String tb_name = new String();
        switch (type) {
            case 1:
                orders = queryUtil.queryNewOilTrasaction();
                tb_name = "oil_transaction_info_2";
                break;
            case 5:
                orders = queryUtil.queryNewOilTrasactionByDemo();
                tb_name = "box_transaction_test";
                break;
            case 3:
                orders = queryUtil.queryNewGoodsTransaction();
                tb_name = "shop_transaction_info_2";
                break;
        }
        addMsgToCache(orders, type);
//        logger.info("scan [{}] thread is running, msg num is [{}], [{}]/per", tb_name, msgContainer.getMsgCount(), beats);
    }

    public void monitor_oil_trasaction_tb() {
        monitor_tb(1);
    }

    public void monitor_transaction_test() {
        monitor_tb(5);
    }

    public void monitor_shop_transaction_info_2() {
        monitor_tb(3);
    }

    void stop() {
        this.isRunning = false;
    }
}
