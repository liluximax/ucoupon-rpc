package cn.ucoupon.rpc.backend.monitor;

import cn.ucoupon.rpc.backend.MsgContainer;
import cn.ucoupon.rpc.backend.SleepUtil;
import cn.ucoupon.rpc.db.QueryUtil;
import cn.ucoupon.rpc.db.UpdateUtil;
import cn.ucoupon.rpc.db.impl.QueryOrderImpl;
import cn.ucoupon.rpc.db.impl.UpdateOrderImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by luc on 17/5/14.
 */
public class MonitorBoxChangedTask extends MonitorTask{

    private static Logger logger = LogManager.getLogger();
    private QueryUtil queryUtil = new QueryOrderImpl();
    private UpdateUtil updateUtil = UpdateOrderImpl.getInstance();
    private MsgContainer msgContainer = MsgContainer.getInstance();
    private static volatile boolean isRunning = true;

    @Override
    void stop() {
        this.isRunning = false;
    }

    @Override
    public void run() {
        while (isRunning){
            int state = queryUtil.queryBoxIdChanged();
            if(state > 0){
                logger.info("the relationship between boxid and station changed");
                msgContainer.removeAllElement();
                updateUtil.updateBoxIdChanged();
            }
            SleepUtil.sleep(2);
        }
    }
}
