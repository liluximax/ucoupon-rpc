package cn.ucoupon.rpc.backend.monitor;

import cn.ucoupon.rpc.backend.BoxMsg;
import cn.ucoupon.rpc.backend.MsgContainer;
import cn.ucoupon.rpc.db.pojo.UcouponMsg;
import com.alibaba.fastjson.JSON;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Created by luc on 17/5/8.
 */
public abstract class MonitorTask implements Runnable{
    private MsgContainer msgContainer = MsgContainer.getInstance();
    private static Logger logger = LogManager.getLogger();

    abstract void stop();

    public void addMsgToCache(List<UcouponMsg> orders, int type){
        if (orders != null || orders.size() > 0) {
            for (UcouponMsg order : orders) {
                int boxId = order.getBox_id();
                long id = order.getId();
                String msgSend = type + "|" + JSON.toJSONString(order);
                String uuid = type + "|" + id;
                BoxMsg msg = new BoxMsg(boxId, msgSend, uuid);
                boolean status = msgContainer.putMsg(msg, uuid);
                if (status) {
                    logger.info("msg [{}] belongs to box[{}] into cache, msg num is [{}]", uuid, boxId, msgContainer.getMsgCount());
                } else {
//                    logger.info("box[{}] [{}] is already stored in cache", boxId, uuid);
                }
            }
        }
    }
}
