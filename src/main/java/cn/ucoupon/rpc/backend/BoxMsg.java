package cn.ucoupon.rpc.backend;

/**
 * Created by luc on 17/5/6.
 * 通过type+id唯一标识一条消息
 * key  type|id
 */
public class BoxMsg {
    private int boxId;
    private String uuid;
    private String msg;

    public BoxMsg() {
    }

    public int getBoxId() {
        return boxId;
    }

    public void setBoxId(int boxId) {
        this.boxId = boxId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public BoxMsg(int boxId, String msg) {
        this.boxId = boxId;
        this.msg = msg;
    }

    public BoxMsg(int boxId, String msg, String uuid) {
        this.boxId = boxId;
        this.msg = msg;
        this.uuid = uuid;
    }
}
