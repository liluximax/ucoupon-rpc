package cn.ucoupon.rpc.backend;

import cn.ucoupon.rpc.db.impl.BoxLogInDbUtil;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import io.netty.channel.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.util.ObjectArrayIterator;

import java.sql.Timestamp;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by luc on 17/5/6.
 * 建立两个KV结构
 * container用于存储 boxid与此box tcp连接的channel关系
 * taskContainer用于存储 channel与推送消息线程的关系
 * channel  --  boxid   --  task
 * 使用boxid为中间量来沟通和标识    特定的消息推送到特定的盒子上
 * <p>
 * 有3个KV结构
 * boxid, boxState
 * channel, boxid   为打log方便
 * channel, task    为了设备断开连接,找到开启的线程,并执行关闭
 */
public class BoxContainer {

    private static MsgContainer msgContainer = MsgContainer.getInstance();

    private static Cache<Integer, BoxState> boxIdContainer = CacheBuilder.newBuilder()
            .expireAfterWrite(60, TimeUnit.SECONDS)
            .removalListener(new RemovalListener<Object, Object>() {
                @Override
                public void onRemoval(RemovalNotification<Object, Object> removalNotification) {
                    String cause = removalNotification.getCause().name();
                    if(cause.equals("REPLACED")){
                        return;
                    }

                    int boxId = (int) removalNotification.getKey();
                    BoxState boxState = (BoxState) removalNotification.getValue();
                    Channel channel = boxState.getChannel();
                    channelContainer.remove(channel);
                    logger.info("remove box[{}] bind info success caused by {}", boxId, cause);

                    boxState.setLogout_time(new Timestamp(System.currentTimeMillis()));
                    boxLogInDbUtil.doOnBoxLogout(boxState);
                    SendMsgTask task = taskContainer.get(channel);
                    task.stop();
                    taskContainer.remove(channel);
                    channel.close();
                }
            }).build();
    private static ConcurrentHashMap<Channel, Integer> channelContainer = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<Channel, SendMsgTask> taskContainer = new ConcurrentHashMap<>();

    private static Logger logger = LogManager.getLogger();
    private static BoxLogInDbUtil boxLogInDbUtil = new BoxLogInDbUtil();
    private static BoxContainer instance = new BoxContainer();

    public static BoxContainer getInstance() {
        return instance;
    }

    public Channel get(Integer boxId) {
        BoxState boxState = boxIdContainer.getIfPresent(boxId);
        if(boxState == null){
            return null;
        }
        return boxState.getChannel();
    }

    public void put(Integer boxId, Channel channel) {
        BoxState boxState = new BoxState(boxId, channel);
        boxState.setLogin_time(new Timestamp(System.currentTimeMillis()));
        boxState.setChannel(channel);
        boxIdContainer.put(boxId, boxState);
        channelContainer.put(channel, boxId);
        logger.info("box[{}] bind channel successful", boxId);
        boxLogInDbUtil.doOnBoxLogin(boxState);
    }

    public Integer getBoxIdByChannel(Channel channel) {
        return channelContainer.getOrDefault(channel, -1);
    }

    public SendMsgTask getTaskByChannel(Channel channel) {
        return taskContainer.get(channel);
    }

    public void putTaskByChannel(Channel channel, SendMsgTask task) {
        taskContainer.put(channel, task);
    }

    public void remove(int boxId) {
        boxIdContainer.invalidate(boxId);
    }

    public boolean checkHeartBeat(int boxId){
        boolean state = boxIdContainer.getIfPresent(boxId) != null ? true : false;
        if(!state){
            boxIdContainer.invalidate(boxId);
        }
        return state;
    }

    public void dealWithHeartBeat(int boxId){
        BoxState boxState = boxIdContainer.getIfPresent(boxId);
        if(boxState != null){
//            logger.info("box[{}] deal with heartbeat, msg num is [{}]", boxId, msgContainer.getMsgCount());
            boxIdContainer.put(boxId, boxState);
        }
    }
}
