package cn.ucoupon.rpc.backend;

import io.netty.channel.Channel;

import java.sql.Timestamp;

/**
 * Created by luc on 17/5/11.
 */
public class BoxState {
    private int box_id;
    private Channel channel;
    private Timestamp login_time;
    private Timestamp logout_time;

    public BoxState() {
    }

    public BoxState(int box_id, Channel channel) {
        this.box_id = box_id;
        this.channel = channel;
    }

    public int getBox_id() {
        return box_id;
    }

    public void setBox_id(int box_id) {
        this.box_id = box_id;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Timestamp getLogin_time() {
        return login_time;
    }

    public void setLogin_time(Timestamp login_time) {
        this.login_time = login_time;
    }

    public Timestamp getLogout_time() {
        return logout_time;
    }

    public void setLogout_time(Timestamp logout_time) {
        this.logout_time = logout_time;
    }
}
