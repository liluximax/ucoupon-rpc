package cn.ucoupon.rpc.db.impl;

import cn.ucoupon.rpc.backend.BoxState;
import cn.ucoupon.rpc.db.DruidConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luc on 17/5/11.
 */
public class BoxLogInDbUtil {
    private static DruidConfig instance = DruidConfig.getInstance();
    private static Logger logger = LogManager.getLogger();

    private static String CHECK_BOX_EXISTENCE = "SELECT count(1) FROM ucoupon.box_login_real_time WHERE box_id = ?";
    private static String INSERT_BOX_LOGIN_INFO = "INSERT INTO ucoupon.box_login_real_time (box_id, state, login_time, logout_time)\n" +
            "  VALUE (?, 0, ?, ?)";
    private static String UPDATE_BOX_LOGIN_INFO = "UPDATE ucoupon.box_login_real_time\n" +
            "SET state = 0, login_time = ?\n" +
            "WHERE box_id = ?";

    private static String UPDATE_BOX_LOGOUT_INFO = "UPDATE ucoupon.box_login_real_time\n" +
            "SET state = 1, logout_time = ?\n" +
            "WHERE box_id = ?";
    private static String RECORD_BOX_LOGIN_LOG = "INSERT INTO ucoupon.box_login_log (box_id, login_time, logout_time) VALUE (?, ?, ?)";

    public void doOnBoxLogin(BoxState boxState) {
        if (boxState == null) {
            logger.error("box login record to db error for null");
        } else {
            int boxId = boxState.getBox_id();
            Connection connection = instance.getConnect();
            PreparedStatement pst = null;
            int count = 0;
            try {
                pst = connection.prepareStatement(CHECK_BOX_EXISTENCE);
                pst.setInt(1, boxState.getBox_id());
                ResultSet rs = pst.executeQuery();
                if (rs.next()) {
                    count = rs.getInt(1);
                }
                //box从未连接过
                if (count == 0) {
                    pst = connection.prepareStatement(INSERT_BOX_LOGIN_INFO);
                    pst.setInt(1, boxState.getBox_id());
                    pst.setTimestamp(2, boxState.getLogin_time());
                    pst.setTimestamp(3, boxState.getLogout_time());
                    boolean flag = pst.execute();
                    logger.info("box[{}] insert into box_login_real_time successful", boxId);
                }
                //更新box_real_time登录状态
                else {
                    pst = connection.prepareStatement(UPDATE_BOX_LOGIN_INFO);
                    pst.setTimestamp(1, boxState.getLogin_time());
                    pst.setInt(2, boxState.getBox_id());
                    int rows = pst.executeUpdate();
                    if (rows == 1) {
                        logger.info("box[{}] login, update login state in box_login_real_time successful", boxId);
                    } else {
                        logger.error("box[{}] login, update login state in box_login_real_time failure", boxId);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    pst.close();
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void doOnBoxLogout(BoxState boxState) {
        if (boxState == null) {
            logger.error("box logout record to db error for null");
        } else {
            int boxId = boxState.getBox_id();
            Connection connection = instance.getConnect();
            PreparedStatement pst = null;
            try {
                pst = connection.prepareStatement(UPDATE_BOX_LOGOUT_INFO);
                pst.setTimestamp(1, boxState.getLogout_time());
                pst.setInt(2, boxId);
                int rows = pst.executeUpdate();
                if (rows == 1) {
                    logger.info("box[{}] logout, update login state in box_login_real_time successful", boxId);
                } else {
                    logger.error("box[{}] logout, update login state in box_login_real_time failure", boxId);
                }

                pst = connection.prepareStatement(RECORD_BOX_LOGIN_LOG);
                pst.setInt(1, boxId);
                pst.setTimestamp(2, boxState.getLogin_time());
                pst.setTimestamp(3, boxState.getLogout_time());
                boolean flag = pst.execute();
                logger.info("box[{}] logout, insert into box_login_log successful", boxId);
            } catch (SQLException e) {
                e.printStackTrace();
            }finally {
                try {
                    pst.close();
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
