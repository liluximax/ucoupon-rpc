package cn.ucoupon.rpc.db.impl;

import cn.ucoupon.rpc.backend.MsgContainer;
import cn.ucoupon.rpc.db.DruidConfig;
import cn.ucoupon.rpc.db.UpdateUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by luc on 17/5/8.
 */
public class UpdateOrderImpl implements UpdateUtil {

    private static UpdateUtil updateUtil = new UpdateOrderImpl();
    private static Logger logger = LogManager.getLogger();
    private static DruidConfig instance = DruidConfig.getInstance();
    private MsgContainer msgContainer = MsgContainer.getInstance();
    public static UpdateUtil getInstance(){
        return updateUtil;
    }

    private static String UPDATE_TRANSACTION_TEST = "UPDATE ucoupon.box_transaction_test SET state = ? , box_get_time = current_timestamp WHERE id = ?";
    private static String UPDATE_OIL_TRANSACTION_INFO_2 = "UPDATE ucoupon.oil_transaction_info_2 SET state = ? , box_get_time = current_timestamp WHERE id = ?";
    private static String UPDATE_SHOP_TRANSACTION_INFO_2 = "UPDATE ucoupon.shop_transaction_info_2 SET state = ? , box_get_time = current_timestamp WHERE id = ?";
    private static String UPDATE_OIL_TRANSACTION_QUERY = "UPDATE ucoupon.oil_transaction_query SET state = ? , box_get_time = current_timestamp WHERE id = ?";
    private static String UPDATE_SHOP_TRANSACTION_QUERY = "UPDATE ucoupon.shop_transaction_query SET state = ? , box_get_time = current_timestamp WHERE id = ?";
    private static final String UPDATE_TRANS_RESTORE = "UPDATE ucoupon.trans_restore SET state = 1 WHERE state = 0";

    @Override
    public void update(long id, int type, int state) {
        Connection connection = instance.getConnect();
        PreparedStatement pst = null;
        String uuid = type + "|" + id;
        try {
            switch (type){
                case 1:
                    pst = connection.prepareStatement(UPDATE_OIL_TRANSACTION_INFO_2);
                    break;
                case 5:
                    pst = connection.prepareStatement(UPDATE_TRANSACTION_TEST);
                    break;
                case 3:
                    pst = connection.prepareStatement(UPDATE_SHOP_TRANSACTION_INFO_2);
                    break;
                case 2:
                    pst = connection.prepareStatement(UPDATE_OIL_TRANSACTION_QUERY);
                    break;
                case 4:
                    pst = connection.prepareStatement(UPDATE_SHOP_TRANSACTION_QUERY);
            }
            if(type >= 1 && type <= 5){
                pst.setInt(1, state);
                pst.setLong(2, id);
            }
            int rows = pst.executeUpdate();
            msgContainer.removeMsgByIdAndType(uuid);
            logger.info("[{}] update affected {} rows", uuid, rows);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                pst.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void updateBoxIdChanged() {
        Connection connection = instance.getConnect();
        PreparedStatement pst = null;
        try {
            pst = connection.prepareStatement(UPDATE_TRANS_RESTORE);
            int rows = pst.executeUpdate();
            logger.info("[{}] update affected {} rows", "trans_restore", rows);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                pst.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
