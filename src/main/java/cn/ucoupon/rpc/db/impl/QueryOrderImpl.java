package cn.ucoupon.rpc.db.impl;

import cn.ucoupon.rpc.db.DruidConfig;
import cn.ucoupon.rpc.db.QueryUtil;
import cn.ucoupon.rpc.db.pojo.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by luc on 17/5/8.
 */
public class QueryOrderImpl implements QueryUtil {

    private static DruidConfig instance = DruidConfig.getInstance();

    private static final String QUERY_OIL_ORDER = "SELECT * FROM ucoupon.oil_transaction_info_2 JOIN ucoupon.station_info USING (station_id) WHERE state = 0";
    private static final String QUERY_OIL_ORDER_DEMO = "SELECT * FROM ucoupon.box_transaction_test WHERE state = 0";
    private static final String QUERY_GOODS_ORDER = "SELECT * FROM ucoupon.shop_transaction_info_2 JOIN ucoupon.station_info USING (shop_id) JOIN ucoupon.shop_info USING (shop_id) WHERE state = 0";
    private static final String QUERY_OIL_TRANSACTION_QUERY = "SELECT\n" +
            "  id,\n" +
            "  box_id,\n" +
            "  station_id,\n" +
            "  station_name,\n" +
            "  start_time,\n" +
            "  end_time\n" +
            "FROM ucoupon.oil_transaction_query\n" +
            "  JOIN ucoupon.station_info USING (station_id)\n" +
            "WHERE state = 0";

    private static String QUERY_PRINT_OIL_TRANSACTION = "SELECT\n" +
            "  transaction_id,\n" +
            "  money,\n" +
            "  user,\n" +
            "  car,\n" +
            "  transaction_time\n" +
            "FROM ucoupon.oil_transaction_info_2\n" +
            "WHERE\n" +
            "  station_id = ? AND print_type = 0 AND transaction_time > ? AND transaction_time < ?\n" +
            "ORDER BY transaction_time ASC";

    private static final String QUERY_GOODS_TRANSACTION_QUERY = "SELECT\n" +
            "  id,\n" +
            "  box_id,\n" +
            "  shop_id,\n" +
            "  shop_name,\n" +
            "  start_time,\n" +
            "  end_time\n" +
            "FROM ucoupon.shop_transaction_query\n" +
            "  JOIN ucoupon.station_info USING (shop_id)\n" +
            "  JOIN ucoupon.shop_info USING (shop_id)\n" +
            "WHERE state = 0";

    private static String QUERY_PRINT_GOODS_TRANSACTION = "SELECT\n" +
            "  transaction_id,\n" +
            "  money,\n" +
            "  user,\n" +
            "  transaction_time\n" +
            "FROM ucoupon.shop_transaction_info_2\n" +
            "WHERE\n" +
            "  shop_id = ? AND print_type = 0 AND transaction_time > ? AND\n" +
            "  transaction_time < ?\n" +
            "ORDER BY transaction_time ASC";

    private static final String QUERY_TRANS_RESTORE = "SELECT count(1) count FROM ucoupon.trans_restore WHERE state = 0";

    public List<UcouponMsg> queryOilOrder(String sql){
        Connection connection = instance.getConnect();
        List<UcouponMsg> res = new ArrayList<>();
        PreparedStatement pst = null;
        try {
            pst = connection.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()){
                OilTrasaction order = new OilTrasaction();
                order.setBox_id(rs.getInt("box_id"));
                order.setId(rs.getLong("id"));
                order.setTransaction_id(rs.getLong("transaction_id"));
                order.setPrint_type(rs.getInt("print_type"));
                order.setStation_id(rs.getString("station_id"));
                order.setStation_name(rs.getString("station_name"));
                order.setMoney(rs.getString("money"));
                order.setUmoney(rs.getString("umoney"));
                order.setVoucher(rs.getString("voucher"));
                order.setUcoupon(rs.getString("ucoupon"));
                order.setCash(rs.getString("cash"));
                order.setPay_type(rs.getString("pay_type"));
                order.setUser(rs.getString("user"));
                order.setCar(rs.getString("car"));
                order.setInvoice(rs.getInt("invoice"));
                order.setInvoice_value(rs.getString("invoice_value"));
                order.setTransaction_time(rs.getString("transaction_time"));
                order.setTransaction_code(rs.getString("transaction_code"));
                res.add(order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                pst.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return res;
    }
    @Override
    public List<UcouponMsg> queryNewOilTrasaction() {
        return queryOilOrder(QUERY_OIL_ORDER);
    }

    @Override
    public List<UcouponMsg> queryNewOilTrasactionByDemo() {
        return queryOilOrder(QUERY_OIL_ORDER_DEMO);
    }

    @Override
    public List<UcouponMsg> queryNewGoodsTransaction() {
        Connection connection = instance.getConnect();
        List<UcouponMsg> res = new ArrayList<>();
        PreparedStatement pst = null;
        try {
            pst = connection.prepareStatement(QUERY_GOODS_ORDER);
            ResultSet rs = pst.executeQuery();
            while (rs.next()){
                GoodsTransaction order = new GoodsTransaction();
                order.setBox_id(rs.getInt("box_id"));
                order.setId(rs.getLong("id"));
                order.setTransaction_id(rs.getLong("transaction_id"));
                order.setPrint_type(rs.getInt("print_type"));
                order.setShop_id(rs.getString("shop_id"));
                order.setShop_name(rs.getString("shop_name"));
                order.setMoney(rs.getString("money"));
                order.setUmoney(rs.getString("umoney"));
                order.setCash(rs.getString("cash"));
                order.setPay_type(rs.getString("pay_type"));
                order.setUser(rs.getString("user"));
                order.setTransaction_time(rs.getString("transaction_time"));
                order.setTransaction_code(rs.getString("transaction_code"));
                res.add(order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                pst.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    @Override
    public List<UcouponMsg> queryOilPrintInfo() {
        List<UcouponMsg> res = new ArrayList<>();
        List<OilQueryMsg> queryList = fetchOilPrint();
        if(queryList.size() > 0){
            for(OilQueryMsg query : queryList){
                String stationId = query.getStation_id();
                String startTime = query.getStart_time();
                String endTime = query.getEnd_time();
                List<Oil> orders = fetchOilPrintOrder(stationId, startTime, endTime);
                int sum_transaction = orders.size();
                double sum_money = 0;
                for(Oil order : orders){
                    sum_money += order.getMoney();
                }
                query.setSum_money(sum_money);
                query.setSum_transaction(sum_transaction);
                query.setList(orders);
                res.add(query);
            }
        }
        return res;
    }

    public List<OilQueryMsg> fetchOilPrint(){
        Connection connection = instance.getConnect();
        List<OilQueryMsg> res = new ArrayList<>();
        PreparedStatement pst = null;
        try {
            pst = connection.prepareStatement(QUERY_OIL_TRANSACTION_QUERY);
            ResultSet rs = pst.executeQuery();
            while (rs.next()){
                OilQueryMsg order = new OilQueryMsg();
                order.setBox_id(rs.getInt("box_id"));
                order.setId(rs.getLong("id"));
                order.setStation_id(rs.getString("station_id"));
                order.setStation_name(rs.getString("station_name"));
                order.setStart_time(rs.getString("start_time"));
                order.setEnd_time(rs.getString("end_time"));
                res.add(order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                pst.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public List<Oil> fetchOilPrintOrder(String statinId, String startTime, String endTime){
        Connection connection = instance.getConnect();
        List<Oil> res = new ArrayList<>();
        PreparedStatement pst = null;
        try {
            pst = connection.prepareStatement(QUERY_PRINT_OIL_TRANSACTION);
            pst.setString(1, statinId);
            pst.setString(2, startTime);
            pst.setString(3, endTime);
            ResultSet rs = pst.executeQuery();
            while (rs.next()){
                Oil oil = new Oil();
                oil.setTransaction_id(rs.getLong("transaction_id"));
                oil.setMoney(rs.getDouble("money"));
                oil.setUser(rs.getString("user"));
                oil.setCar(rs.getString("car"));
                oil.setTransaction_time(rs.getString("transaction_time"));
                res.add(oil);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                pst.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    @Override
    public List<UcouponMsg> queryGoodsPrintInfo() {
        List<UcouponMsg> res = new ArrayList<>();
        List<GoodsQueryMsg> queryList = fetchGoodsPrint();
        if(queryList.size() > 0){
            for(GoodsQueryMsg query : queryList){
                String shopId = query.getShop_id();
                String startTime = query.getStart_time();
                String endTime = query.getEnd_time();
                List<Goods> orders = fetchGoogsPrintOrder(shopId, startTime, endTime);
                int sum_transaction = orders.size();
                double sum_money = 0;
                for(Goods order : orders){
                    sum_money += order.getMoney();
                }
                query.setSum_money(sum_money);
                query.setSum_transaction(sum_transaction);
                query.setList(orders);
                res.add(query);
            }
        }
        return res;
    }

    public List<GoodsQueryMsg> fetchGoodsPrint(){
        Connection connection = instance.getConnect();
        List<GoodsQueryMsg> res = new ArrayList<>();
        PreparedStatement pst = null;
        try {
            pst = connection.prepareStatement(QUERY_GOODS_TRANSACTION_QUERY);
            ResultSet rs = pst.executeQuery();
            while (rs.next()){
                GoodsQueryMsg order = new GoodsQueryMsg();
                order.setBox_id(rs.getInt("box_id"));
                order.setId(rs.getLong("id"));
                order.setShop_id(rs.getString("shop_id"));
                order.setShop_name(rs.getString("shop_name"));
                order.setStart_time(rs.getString("start_time"));
                order.setEnd_time(rs.getString("end_time"));
                res.add(order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                pst.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public List<Goods> fetchGoogsPrintOrder(String shopId, String startTime, String endTime){
        Connection connection = instance.getConnect();
        List<Goods> res = new ArrayList<>();
        PreparedStatement pst = null;
        try {
            pst = connection.prepareStatement(QUERY_PRINT_GOODS_TRANSACTION);
            pst.setString(1, shopId);
            pst.setString(2, startTime);
            pst.setString(3, endTime);
            ResultSet rs = pst.executeQuery();
            while (rs.next()){
                Goods goods = new Goods();
                goods.setTransaction_id(rs.getLong("transaction_id"));
                goods.setMoney(rs.getDouble("money"));
                goods.setUser(rs.getString("user"));
                goods.setTransaction_time(rs.getString("transaction_time"));
                res.add(goods);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                pst.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    @Override
    public int queryBoxIdChanged() {
        Connection connection = instance.getConnect();
        int count = 0;
        PreparedStatement pst = null;
        try {
            pst = connection.prepareStatement(QUERY_TRANS_RESTORE);
            ResultSet rs = pst.executeQuery();
            if(rs.next()){
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                pst.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return count;
    }
}
