package cn.ucoupon.rpc.db;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by luc on 17/5/8.
 */
public class DruidConfig {

    private static Logger logger = LogManager.getLogger();
    private static DruidConfig instance = new DruidConfig();

    private static DruidDataSource dss = null;

    private DruidConfig() {

    }

    static {
        Properties properties = new Properties();
        InputStream in = DruidConfig.class.getClassLoader().getResourceAsStream("db.properties");
        try {
            properties.load(in);
            dss = (DruidDataSource) DruidDataSourceFactory.createDataSource(properties);
            logger.info("fetch mysql connect successful");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static DruidConfig getInstance(){
        return instance;
    }

    public Connection getConnect(){
        Connection connection = null;
        try {
            connection = dss.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

}
