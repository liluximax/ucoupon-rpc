package cn.ucoupon.rpc.db;

/**
 * Created by luc on 17/5/8.
 */
public interface UpdateUtil {
    void update(long id, int type, int state);

    void updateBoxIdChanged();
}
