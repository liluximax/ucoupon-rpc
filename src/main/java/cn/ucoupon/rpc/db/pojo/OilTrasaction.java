package cn.ucoupon.rpc.db.pojo;

/**
 * Created by luc on 17/5/8.
 * 油品交易订单数据类
 * box_transaction_test
 * oil_transaction_info_2
 */
public class OilTrasaction extends Transaction{

    private String station_id;
    private String station_name;
    private String car;
    private int invoice;
    private String invoice_value;

    public OilTrasaction() {
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public int getInvoice() {
        return invoice;
    }

    public void setInvoice(int invoice) {
        this.invoice = invoice;
    }

    public String getInvoice_value() {
        return invoice_value;
    }

    public void setInvoice_value(String invoice_value) {
        this.invoice_value = invoice_value;
    }

    public String getStation_id() {
        return station_id;
    }

    public void setStation_id(String station_id) {
        this.station_id = station_id;
    }

    public String getStation_name() {
        return station_name;
    }

    public void setStation_name(String station_name) {
        this.station_name = station_name;
    }

}
