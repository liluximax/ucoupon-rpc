package cn.ucoupon.rpc.db.pojo;

/**
 * Created by luc on 17/5/9.
 */
public class Transaction extends UcouponMsg{

    private long transaction_id;
    private int print_type;
    private String money;
    private String umoney;
    private String voucher;
    private String ucoupon;
    private String cash;
    private String pay_type;
    private String user;
    private String transaction_time;
    private String transaction_code;

    public Transaction() {
    }

    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getPay_type() {
        return pay_type;
    }

    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }

    public int getPrint_type() {
        return print_type;
    }

    public void setPrint_type(int print_type) {
        this.print_type = print_type;
    }

    public String getTransaction_code() {
        return transaction_code;
    }

    public void setTransaction_code(String transaction_code) {
        this.transaction_code = transaction_code;
    }

    public long getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(long transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getTransaction_time() {
        return transaction_time;
    }

    public void setTransaction_time(String transaction_time) {
        this.transaction_time = transaction_time;
    }

    public String getUcoupon() {
        return ucoupon;
    }

    public void setUcoupon(String ucoupon) {
        this.ucoupon = ucoupon;
    }

    public String getUmoney() {
        return umoney;
    }

    public void setUmoney(String umoney) {
        this.umoney = umoney;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }
}
