package cn.ucoupon.rpc.db.pojo;

/**
 * Created by luc on 17/5/9.
 */
public abstract class UcouponMsg {
    private int box_id;
    private long id;

    public UcouponMsg() {
    }

    public int getBox_id() {
        return box_id;
    }

    public void setBox_id(int box_id) {
        this.box_id = box_id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


}
