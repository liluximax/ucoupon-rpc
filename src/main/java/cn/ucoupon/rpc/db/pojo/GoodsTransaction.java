package cn.ucoupon.rpc.db.pojo;

/**
 * Created by luc on 17/5/9.
 */
public class GoodsTransaction extends Transaction {
    private String shop_id;
    private String shop_name;

    public GoodsTransaction() {
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }
}
