package cn.ucoupon.rpc.db.pojo;

import java.util.List;

/**
 * Created by luc on 17/5/10.
 */
public class GoodsQueryMsg extends UcouponMsg {
    private String shop_id;
    private String shop_name;
    private String start_time;
    private String end_time;
    private double sum_money;
    private int sum_transaction;
    private List<Goods> list;

    public GoodsQueryMsg() {
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public List<Goods> getList() {
        return list;
    }

    public void setList(List<Goods> list) {
        this.list = list;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public double getSum_money() {
        return sum_money;
    }

    public void setSum_money(double sum_money) {
        this.sum_money = sum_money;
    }

    public int getSum_transaction() {
        return sum_transaction;
    }

    public void setSum_transaction(int sum_transaction) {
        this.sum_transaction = sum_transaction;
    }
}
