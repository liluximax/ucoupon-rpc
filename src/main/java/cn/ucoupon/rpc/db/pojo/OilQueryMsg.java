package cn.ucoupon.rpc.db.pojo;

import com.alibaba.fastjson.annotation.JSONType;

import java.util.List;

/**
 * Created by luc on 17/5/9.
 */
@JSONType(orders = {"station_id", "statioin_name", "start_time", "end_time", "sum_money", "sum_tansaction", "list"})
public class OilQueryMsg extends UcouponMsg{
    private String station_id;
    private String station_name;
    private String start_time;
    private String end_time;
    private double sum_money;
    private int sum_transaction;
    private List<Oil> list;

    public OilQueryMsg() {
    }

    public List<Oil> getList() {
        return list;
    }

    public void setList(List<Oil> list) {
        this.list = list;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getStation_id() {
        return station_id;
    }

    public void setStation_id(String station_id) {
        this.station_id = station_id;
    }

    public String getStation_name() {
        return station_name;
    }

    public void setStation_name(String station_name) {
        this.station_name = station_name;
    }

    public double getSum_money() {
        return sum_money;
    }

    public void setSum_money(double sum_money) {
        this.sum_money = sum_money;
    }

    public int getSum_transaction() {
        return sum_transaction;
    }

    public void setSum_transaction(int sum_transaction) {
        this.sum_transaction = sum_transaction;
    }
}
