package cn.ucoupon.rpc.db;

import cn.ucoupon.rpc.db.pojo.OilTrasaction;
import cn.ucoupon.rpc.db.pojo.UcouponMsg;

import java.util.List;

/**
 * Created by luc on 17/5/8.
 */
public interface QueryUtil {
    //查询最新油品交易信息
    List<UcouponMsg> queryNewOilTrasaction();

    //查询最新测试交易信息
    List<UcouponMsg> queryNewOilTrasactionByDemo();

    //查询最新非油品交易信息
    List<UcouponMsg> queryNewGoodsTransaction();

    //油品查询打印消息
    List<UcouponMsg> queryOilPrintInfo();

    //非油品查询打印消息
    List<UcouponMsg> queryGoodsPrintInfo();

    //查询box与绑定油站关系发生变化时间的发生
    //如果绑定关系发生变化,立即清除缓存中所有数据
    //下一个监控表线层对数据进行重读
    int queryBoxIdChanged();
}
