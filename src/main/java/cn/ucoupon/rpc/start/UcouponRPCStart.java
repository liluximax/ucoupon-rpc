package cn.ucoupon.rpc.start;

import cn.ucoupon.rpc.backend.SleepUtil;
import cn.ucoupon.rpc.backend.monitor.MonitorBoxChangedTask;
import cn.ucoupon.rpc.backend.monitor.MonitorOilOrderTask;
import cn.ucoupon.rpc.backend.monitor.MonitorPrintTask;
import cn.ucoupon.rpc.backend.monitor.MonitorTask;
import cn.ucoupon.rpc.server.MyServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by luc on 17/5/8.
 */
public class UcouponRPCStart {

    private static Logger logger = LogManager.getLogger();

    public static void main(String[] args) throws InterruptedException {
        ExecutorService threadPool = Executors.newFixedThreadPool(10);
        //监控油品交易线程
        MonitorTask monitorTask1 = new MonitorOilOrderTask();
        threadPool.execute(monitorTask1);
        logger.info("start transaction monitor successful");
        SleepUtil.sleep(2);
        //监控查询打印线程
        MonitorTask monitorTask2 = new MonitorPrintTask();
        threadPool.execute(monitorTask2);
        logger.info("start print monitor successful");
        //监控boxid与station绑定关系线程
        MonitorTask monitorTask3 = new MonitorBoxChangedTask();
        threadPool.execute(monitorTask3);
        logger.info("start trans_changed monitor successful");

        SleepUtil.sleep(3);
        MyServer server = new MyServer();
        server.run();
        logger.info("start tcp server successful");
    }
}
