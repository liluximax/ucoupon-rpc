package cn.ucoupon.rpc.server;

import cn.ucoupon.rpc.backend.BoxContainer;
import cn.ucoupon.rpc.backend.MsgContainer;
import cn.ucoupon.rpc.backend.SendMsgTask;
import cn.ucoupon.rpc.db.UpdateUtil;
import cn.ucoupon.rpc.db.impl.UpdateOrderImpl;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

/**
 * Created by luc on 17/5/6.
 */
public class ChatServerHandler extends SimpleChannelInboundHandler<String> {

    private BoxContainer boxContainer = BoxContainer.getInstance();
    private static ExecutorService service = Executors.newFixedThreadPool(100);
    private static final Logger logger = LogManager.getLogger();
    private static final String NUM_REGEX = "[0-9]+";
    private static final String TICKET_REGEX = "^[0-9]\\|[0-9]\\|[0-9]+$";
    private static UpdateUtil updateUtil = UpdateOrderImpl.getInstance();

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg)
            throws Exception {
        Channel channel = ctx.channel();
        int boxId = boxContainer.getBoxIdByChannel(channel);
        msg = msg.trim();
        logger.info("reveive box[" + boxId + "] : " + msg);
        //执行boxid绑定channel逻辑
        if (Pattern.matches(NUM_REGEX, msg)) {
            int boxIdMsg = Integer.valueOf(msg);
            if (boxId == -1) {
                try {
                    Channel channelSave = boxContainer.get(boxIdMsg);
                    if (null == channelSave) {
                        boxContainer.put(boxIdMsg, channel);
                        SendMsgTask task = new SendMsgTask(boxIdMsg);
                        boxContainer.putTaskByChannel(channel, task);
                        service.execute(task);
                    } else {
                        logger.info("box["+ boxIdMsg + "] is already bind a channel");
                    }

                } catch (Exception e) {
                    logger.debug("[" + boxId + "] msg is not number in bind stage");
                }
            }
            //已绑定的盒子 处理维持连接的心跳消息
            else if(boxId == boxIdMsg){
                boxContainer.dealWithHeartBeat(boxId);
            }
            else {
                logger.error("box[{}] send error heartbeat", boxIdMsg);
            }
        }
        //处理推送订单消息后,盒子主动回复的消息
        //消息格式  0|type|id   0成功 1失败
        else if (Pattern.matches(TICKET_REGEX, msg)) {
            String[] response = msg.split("\\|");
            if (response.length == 3) {
                int status = Integer.valueOf(response[0]);
                int type = Integer.valueOf(response[1]);
                long id = Long.valueOf(response[2]);
                String uuid = type + "|" + id;
                //0代表出票成功,将state改写成1
                if (status == 0) {
                    updateUtil.update(id, type, 1);
                }
                //出票失败,将state改写成2
                else {
                    updateUtil.update(id, type, 2);
                    logger.error("box[{}] respond [{}] error", boxId, uuid);
                }
            }
        }
        channel.writeAndFlush("0\n");
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        Channel channel = ctx.channel();
        int boxId = boxContainer.getBoxIdByChannel(channel);
        if (boxId != -1) {
            logger.info("[" + boxId + "] " + "online");
        } else {
            logger.info("[" + channel.remoteAddress() + "] " + "first online, cannot get boxid");
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        Channel channel = ctx.channel();
        int boxId = boxContainer.getBoxIdByChannel(channel);
        if (boxId != -1) {
            logger.info("[" + boxId + "] " + "offline");
            SendMsgTask task = boxContainer.getTaskByChannel(channel);
            boxContainer.remove(boxId);
            task.stop();
        } else {
            logger.info("[" + channel.remoteAddress() + "] " + "offline, the box didn't send boxid anymore");
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
            throws Exception {
        Channel channel = ctx.channel();
        int boxId = boxContainer.getBoxIdByChannel(channel);
        if (boxId != -1) {
            logger.error("[" + boxId + "] " + "offline by error");
            SendMsgTask task = boxContainer.getTaskByChannel(channel);
            boxContainer.remove(boxId);
            task.stop();
        } else {
            logger.error("[" + ctx.channel().remoteAddress() + "]" + "exit by error");
        }
        ctx.close().sync();
    }
}
