import cn.ucoupon.rpc.backend.BoxState;
import cn.ucoupon.rpc.backend.SleepUtil;
import cn.ucoupon.rpc.db.QueryUtil;
import cn.ucoupon.rpc.db.UpdateUtil;
import cn.ucoupon.rpc.db.impl.BoxLogInDbUtil;
import cn.ucoupon.rpc.db.impl.QueryOrderImpl;
import cn.ucoupon.rpc.db.impl.UpdateOrderImpl;
import cn.ucoupon.rpc.db.pojo.OilQueryMsg;
import cn.ucoupon.rpc.db.pojo.OilTrasaction;
import cn.ucoupon.rpc.db.pojo.UcouponMsg;
import com.alibaba.fastjson.JSON;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by luc on 17/5/8.
 */
public class DBTest {
    private static Logger logger = LogManager.getLogger();
    public static void main(String[] args) {
        QueryUtil queryUtil = new QueryOrderImpl();
        System.out.println(queryUtil.queryBoxIdChanged());
    }
}
